
## Setup process

This project was developed in laravel/hometead environment by Abu Bakkar Siddique.

Install Composer Packages

```bash
  composer install
```

Install Node Packages

```bash
  npm install

  npm run dev
```
