<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function store(Request $request, Project $project)
    {
        $request->validate([
            'name' => 'required|max:120'
        ]);

        $task = new Task($request->only(['name']));
        $task->priority = $project->tasks()->count() + 1;
        $project->tasks()->save($task);

        return redirect()->route('projects.show', [$project->id]);
    }

    public function update(Request $request, Project $project, Task $task)
    {
        $request->validate([
            'name' => 'required|max:120'
        ]);

        $task->name = $request->get('name');
        $task->save();

        return redirect()->route('projects.show', [$project->id]);
    }

    public function destroy(Project $project, Task $task)
    {
        $task->delete();

        return redirect()->route('projects.show', [$project->id]);
    }
}
