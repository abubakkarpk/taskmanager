<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Project;
use Illuminate\Http\Request;

class TaskPriorityController extends Controller
{
    function __invoke(Request $request, Project $project)
    {
        $request->validate([
            'ids' => 'required|array'
        ]);

        $ids = $request->ids;

        foreach($ids as $index => $id) {
            $task = $project->tasks()->find($id);
            $task->priority = $index + 1;
            $task->save();
        }

        return response()->json([]);
    }
}
