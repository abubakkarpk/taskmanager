<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function show(Request $request, Project $project)
    {
        $editId = $request->get('edit_task_id', 0);
        $deleteId = $request->get('delete_task_id', 0);
        $tasks = $project->tasks()->orderBy('priority')->get();

        return view('project', compact('project', 'tasks', 'editId', 'deleteId'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:120'
        ]);

        $project = new Project($request->only(['name']));
        $project->save();

        return redirect()->route('home');
    }

    public function update(Request $request, Project $project)
    {
        $request->validate([
            'name' => 'required|max:120'
        ]);

        $project->name = $request->get('name');
        $project->save();

        return redirect()->route('home');
    }

    public function destroy(Project $project)
    {
        $project->delete();

        return redirect()->route('home');
    }
}
