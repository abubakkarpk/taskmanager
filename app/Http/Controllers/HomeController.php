<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $editId = $request->get('edit_project_id', 0);
        $deleteId = $request->get('delete_project_id', 0);

        $projects = Project::all();
        return view('home', compact('projects', 'editId', 'deleteId'));
    }   
}
