<?php

namespace App\View\Components;

use App\Models\Project;
use App\Models\Task;
use Illuminate\View\Component;

class DeleteTaskForm extends Component
{
    public $project;
    public $task;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(Project $project, Task $task)
    {
        $this->project = $project;
        $this->task = $task;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.delete-task-form');
    }
}
