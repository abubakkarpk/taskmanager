<?php

namespace App\View\Components;

use App\Models\Project;
use Illuminate\View\Component;

class CreateTaskForm extends Component
{
    public $project;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(Project $project)
    {
        $this->project = $project;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.create-task-form');
    }
}
