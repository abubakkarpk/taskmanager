@extends('layout.app')

@section('title', $project->name)

@section('page')
<div>
  <div class="d-flex justify-content-between align-items-center">
    <h3 class="h3 text-muted font-weight-bold">{{$project->name}}'s Tasks</h3>
    <a href="{{route('home')}}">Go Back</a>
  </div>
  <x-create-task-form :project="$project"/>
  <div>
    <div class="list-group" id="sortable">
      @foreach($tasks as $task)
      @if($editId != $task->id && $deleteId != $task->id)
      <div class="list-group-item py-0" id="{{$task->id}}">
        <div class="d-flex justify-content-between align-items-center">
          <div>
            {{$task->name}}
          </div>
          <div class="btn-group">
            <a href="{{route('projects.show', [$project->id, 'edit_task_id'=>$task->id])}}" class="btn btn-link text-primary">Edit</a>
            <a href="{{route('projects.show', [$project->id, 'delete_task_id'=>$task->id])}}" class="btn btn-link text-danger">Delete</a>
          </div>
        </div>
      </div>
      @elseif($editId == $task->id)
        <x-update-task-form :project="$project" :task="$task"/>
      @elseif($deleteId == $task->id)
        <x-delete-task-form :project="$project" :task="$task"/>
      @endif
      @endforeach
    </div>
  </div>
</div>

@endsection