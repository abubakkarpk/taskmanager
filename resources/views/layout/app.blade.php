<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>@yield('title') | Task Manager</title>
  <link rel="stylesheet" href="{{mix('/css/app.css')}}">
</head>
<body>
  <div class="container mt-3">
    <div class="row">
      <div class="col-10 offset-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
        @yield('page')
      </div>
    </div>
  </div>
  <script src="{{ mix('/js/app.js') }}"></script>
  <script>
    $( function() {
      $( "#sortable" ).sortable({
        stop: function(e, ui) {
          let ids = $(this).sortable('toArray');
          let projectId = $('#projectId').val();
          $.ajax({
            method: 'PUT',
            url: `/api/projects/${projectId}/tasks/priorities`,
            data: {ids}
          })
        }
      });
    });
    </script>
</body>
</html>