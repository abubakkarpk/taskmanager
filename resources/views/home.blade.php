@extends('layout.app')

@section('title', 'Home')

@section('page')
<div>
  <h3 class="h3 text-muted font-weight-bold">All Projects</h3>
  <x-create-project-form/>
  <div>
    <div class="list-group">
      @foreach($projects as $project)
      @if($editId != $project->id && $deleteId != $project->id)
      <div class="list-group-item py-0">
        <div class="d-flex justify-content-between align-items-center">
          <a href="{{route('projects.show', [$project->id])}}">
            {{$loop->iteration}} - {{$project->name}}
            ({{$project->tasks()->count()}} tasks)
          </a>
          <div class="btn-group">
            <a href="{{route('home', ['edit_project_id'=>$project->id])}}" class="btn btn-link text-primary">Edit</a>
            <a href="{{route('home', ['delete_project_id'=>$project->id])}}" class="btn btn-link text-danger">Delete</a>
          </div>
        </div>
      </div>
      @elseif($editId == $project->id)
        <x-update-project-form :project="$project"/>
      @elseif($deleteId == $project->id)
        <x-delete-project-form :project="$project"/>
      @endif
      @endforeach
    </div>
  </div>
</div>
@endsection