<form action="{{route('projects.store')}}" method="POST">
  @csrf
  @method('POST')
  <div class="row no-gutters">
    <div class="col-12 col-md-9">
      <input 
        name="name" 
        class="form-control @error('name') is-invalid @enderror" 
        placeholder="New Project"
      >
    </div>
    <div class="col-12 col-md-3">
      <button type="submit" class="btn btn-success btn-block">Create</button>
    </div>
  </div>
</form>