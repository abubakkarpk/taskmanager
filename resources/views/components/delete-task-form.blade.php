<form action="{{route('projects.tasks.destroy', [$project->id, $task->id])}}" method="POST">
  @csrf
  @method('DELETE')
  <div class="d-flex justify-content-between align-items-center">
    <div class="text-danger">
      Delete <b>{{$task->name}}</b>
    </div>
    <div class="d-flex">
      <a href="{{route('projects.show', [$project->id])}}" class="btn btn-link text-primary">Cancel</a>
      <button type="submit" class="btn btn-danger btn-block">Confirm</button>
    </div>
  </div>
</form>