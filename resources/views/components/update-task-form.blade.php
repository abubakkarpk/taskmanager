<form action="{{route('projects.tasks.update', [$project->id, $task->id])}}" method="POST">
  @csrf
  @method('PUT')
  <div class="row no-gutters">
    <div class="col-12 col-md-8">
      <input 
        name="name" 
        class="form-control @error('name') is-invalid @enderror" 
        placeholder="Task Name"
        value="{{$task->name}}"
      >
    </div>
    <div class="col-12 col-md-4 d-flex">
      <a href="{{route('projects.show', [$project->id])}}" class="btn btn-link text-primary">Cancel</a>
      <button type="submit" class="btn btn-primary btn-block">Update</button>
    </div>
  </div>
</form>