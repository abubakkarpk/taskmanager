<form action="{{route('projects.update', [$project->id])}}" method="POST">
  @csrf
  @method('PUT')
  <div class="row no-gutters">
    <div class="col-12 col-md-8">
      <input 
        name="name" 
        class="form-control @error('name') is-invalid @enderror" 
        placeholder="Project Name"
        value="{{$project->name}}"
      >
    </div>
    <div class="col-12 col-md-4 d-flex">
      <a href="{{route('home')}}" class="btn btn-link text-primary">Cancel</a>
      <button type="submit" class="btn btn-primary btn-block">Update</button>
    </div>
  </div>
</form>