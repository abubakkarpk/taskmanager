<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::post('/projects', 'ProjectController@store')->name('projects.store');
Route::get('/projects/{project}', 'ProjectController@show')->name('projects.show');
Route::put('/projects/{project}', 'ProjectController@update')->name('projects.update');
Route::delete('/projects/{project}', 'ProjectController@destroy')->name('projects.destroy');


Route::prefix('/projects/{project}')->name('projects.')->group(function() {
    Route::post('/tasks', 'TaskController@store')->name('tasks.store');
    Route::put('/tasks/{task}', 'TaskController@update')->name('tasks.update');
    Route::delete('/tasks/{task}', 'TaskController@destroy')->name('tasks.destroy');
});