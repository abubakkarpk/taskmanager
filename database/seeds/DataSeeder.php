<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $projects = [
            'Laravel' => [
                'Setup new Project',
                'Create database tables and models',
                'Create routes',
                'Make controllers',
                'Create templates',
                'Test application',
                'Deploy application',
            ],
            'Shopping' => [
                '1 Milk Bread',
                '12 Eggs',
                '1 KG Potatos',
            ]
        ];

        foreach($projects as $name => $tasks) {
            $id = DB::table('projects')->insertGetId([
                'name' => $name,
                'created_at' => now(),
                'updated_at' => now(),
            ]);

            foreach($tasks as $index => $task) {
                DB::table('tasks')->insert([
                    'name' => $task,
                    'priority' => $index + 1,
                    'project_id' => $id,
                    'created_at' => now(),
                    'updated_at' => now(),
                ]);
            }
        }
    }
}
